/**
 * Topic CV Package.
 */
var tcp = {};
tcp.onChoice = null;
tcp.onArray = {};
tcp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(tcp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) tcp.onChoice = incNav.onArray[i];
		}
		tcp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(tcp.onChoice == null) tcp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(tcp.createTag());


};
tcp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < tcp.onChoice.layer; i++){
			layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(tcp.onChoice.index == 0) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(tcp.onArray._template.off) result += 'off '; off処理
	// next back
	// if(tcp.onChoice.back != '') result += '<li class="back"><a href="../' + tcp.onChoice.back + '/index.html">BACK</a></li>';
	// if(tcp.onChoice.next != '') result += '<li class="next"><a href="../' + tcp.onChoice.next + '/index.html">NEXT</a></li>';

	result += '<div class="btmCv">';
	result += '<div class="wrap">';
	result += '<p class="btmCv__lead">物件エントリー受付開始</p>';
	result += '<p class="btmCv__text">物件エントリーされた方は、限定ページで未公開間取りをご覧いただけます。<br>' +
        'また、販売スケジュールやエリアガイドなどの最新の情報をお届けいたします。<br>' +
        'さらに、モデルルームの優先予約特典つきです！</p>';
	result += '<div class="btmCv__btn btn blockMarker-box">';
	result += '<span class="blockMarkerMsk" data-aos="blockMarker"></span>';
	result += '<div href="" data-aos="blockMarkerCnt">';
	result += '<a href="#">物件エントリー</a>';
	result += '</div>';
	result += "</div><!-- / .blockMarker-box -->";
	result += '</div><!-- / .wrap -->';

	if(tcp.onChoice.index !== 10) {
		result += '<div class="toPrevPage blockMarker-box btn">';
		result += '<span class="blockMarkerMsk color2" data-aos="blockMarkerR"></span>';
		result += '<a href="../' + tcp.onChoice.back + '/index.html">'+ tcp.onChoice.back.toUpperCase() +'</a>';
		result += '</div>';
		result += '<div class="toNextPage blockMarker-box btn"></a>';
		result += '<span class="blockMarkerMsk color2" data-aos="blockMarker"></span>';
		result += '<a href="../' + tcp.onChoice.next + '/index.html">'+ tcp.onChoice.next.toUpperCase() +'</a>';
		result += '</div>';
	}
	result += "</div><!-- / .wrap -->";
	result += "</div><!-- / .btmCv -->";

	result += '<div class="btmCap">';
	result += '<div class="wrap">';
	if (tcp.onChoice.index == 0) {
		result += '<p class="caption">※1.総戸数38戸中26戸が南向き<br>' +
            '※掲載の外観完成予想図は計画段階の図面を基に描いたもので、実際とは異なります。また、今後変更になる場合があります。なお、周辺建物および外観の細部・設備機器・配管類等は一部省略又は簡略化しております。植栽につきましては特定の季節の状況を表現したものではなく、竣工時には完成予想図程度には成長しておりません。タイルや各種部材いつきましては、実際と質感・色等の見え方が異なる場合があります。<br>' +
            '※掲載の施設・環境写真は平成30年8月に撮影したものです。<br>' +
            '※掲載の徒歩分数は80m=1分として算出(端数切り上げ)したものです。<br>' +
            '※掲載の情報は平成30年9月現在のものであり今後変更になる場合があります。</p>';
	}
	if (tcp.onChoice.index == 1) {
		result += '<p class="caption">※1.当社単独事業の実績として\n' +
            '※2. 東京急行電鉄株式会社ニュースリリース（2017年4月18日）門前町・池上の玄関口である池上駅の駅舎改良および駅ビル開発計画が始動」より<br>' +
            '※掲載のポジション概念図は、立地状況を説明するための概念図であり、距離・縮尺等は実際とは異なります。<br>' +
            '※掲載の施設・環境写真は平成30年8月に撮影したものです。<br>' +
            '※掲載の徒歩分数は80m=1分として算出(端数切り上げ)したものです。<br>' +
            '※掲載の情報は平成30年9月現在のものであり今後変更になる場合があります。<br>' +
            '※掲載の外観完成予想図は計画段階の図面を基に描いたもので、実際とは異なります。また、今後変更になる場合があります。なお、周辺建物および外観の細部・設備機器・配管類等は一部省略又は簡略化しております。植栽につきましては特定の季節の状況を表現したものではなく、竣工時には完成予想図程度には成長しておりません。タイルや各種部材いつきましては、実際と質感・色等の見え方が異なる場合があります。<br>' +
            '※「品川」駅11分（通勤時：12分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換。<br>' +
            '※「横浜」駅13分（通勤時：14分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換、「川崎」駅よりJR東海道本線に乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換、「川崎」駅よりJR東海道本線に乗換。）</p>';
	}
	if (tcp.onChoice.index == 2) {
		result += '<p class="caption">※掲載の施設・環境写真は平成30年7・8月に撮影したものです。<br>' +
            '※掲載の徒歩分数は80m=1分として算出(端数切り上げ)したものです。<br>' +
            '※掲載の情報は平成30年9月現在のものであり今後変更になる場合があります。<br>' +
            '※「渋谷」駅23分（通勤時：24分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換、「品川」駅よりJR山手線乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換、「品川」駅よりJR山手線乗換。）' +
            '※「蒲田」駅2分（通勤時：2分）「蓮沼」駅より東急池上線利用。<br>' +
            '※「五反田」駅20分（通勤時：21分）「池上」駅より東急池上線利用。<br>' +
            '※「品川」駅11分（通勤時：12分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換。<br>' +
            '※「川崎」駅6分（通勤時：6分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換。<br>' +
            '※「横浜」駅13分（通勤時：14分）「蓮沼」駅より東急池上線利用、「蒲田」駅よりJR京浜東北線快速乗換、「川崎」駅よりJR東海道本線に乗換。（通勤時は「蒲田」駅よりJR京浜東北線乗換、「川崎」駅よりJR東海道本線に乗換。）<br>' +
            '※「多摩川」駅8分（通勤時：9分）「矢口渡」駅より東急多摩川線利用。<br>' +
            '※「武蔵小杉」駅11分（通勤時：11分）「矢口渡」駅より東急多摩川線利用、「多摩川」駅より東急東横線急行利用。「蒲田」駅2分（通勤時：2分）「蓮沼」駅より東急池上線利用。<br>' +
            '※タクシー料金は金額の目安であり、経路や道路状況等により異なります。<br>' +
            '※東京ハイヤー・タクシー協会（http://www.taxi-tokyo.or.jp/index.html）より算出</p>';
	}
	if (tcp.onChoice.index == 3) {
		result += '<p class="caption">※掲載のイラストは地図や航空写真を基に概念的に描き起こしたもので実際とは異なります。<br>' +
            '※掲載の施設・環境写真は平成30年7・8月に撮影したものです。<br>' +
            '※掲載の徒歩分数は80m=1分として算出(端数切り上げ)したものです。<br>' +
            '※掲載の情報は平成30年9月現在のものであり今後変更になる場合があります。</p>';
	}
	if (tcp.onChoice.index == 5) {
		result += '<p class="caption">※掲載の間取り図は、計画段階の図面を基に描いたもので今後変更になる場合があります。<br>' +
            '※無償セレクト・ワンポイントオーダー（有償）・オーダーメイド（有償）には対応住戸や申込期限に制限があります。詳細につきましては、係員までお問い合わせください。</p>';
	}
	if (tcp.onChoice.index == 7) {
		result += '<p class="caption">※掲載の徒歩分数は80m=1分として算出(端数切り上げ)したものです。<br>' +
			'※徒歩分数は、池上駅出口からサブエントランス口まで。<br>' +
            '※掲載の施設・環境写真は平成30年8月に撮影したものです。</p>';
	}
	if (tcp.onChoice.index == 9) {
		result += '<p class="caption">※徒歩分数は、池上駅出口からサブエントランス口まで。<br>' +
            '※掲載の施設・環境写真は平成30年8月に撮影したものです</p>';
	}
	result += "</div><!-- / .wrap -->";
	result += "</div><!-- / .btmCap -->";

	result += '<nav class="list-property-control-wrap only-sp">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item contact">';
	result += '<a href="tel:0120313038" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon contact"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="お問い合わせ"></i> <span class="list-property-control-text">お問い合わせ</span> </a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="' + layer + 'map/index.html" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon localmap"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="現地案内図"></i>';
	result += '<span class="list-property-control-text">現地案内図</span>';
	result += '</a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"><img src="https://www.proud-web.jp/general/img/ico_book_01_sp.png" class="only-sp" alt="物件エントリー"></i> <span class="list-property-control-text">物件エントリー</span> </a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="' + layer + 'limited/login.html" class="list-property-control-link list-property-control-link-wine">';
	result += '<i class="list-property-control-icon request"><img src="https://www.proud-web.jp/general/img/ico_request_01_sp.png" class="only-sp" alt="物件エントリー者様限定サイト"></i> <span class="list-property-control-text">物件エントリー者様限定サイト</span> </a>';
	result += '</li>';
	result += '</ul>';
	result += '</nav>';

	result += '<div class="box-bukken-contact">';
	result += '<div class="box-bukken-contact-inner">';
	result += '<ul class="list-bukken-contact">';
	result += '<li class="list-bukken-contact-item entry">';
	result += '<a href="#" class="list-bukken-contact-link list-bukken-contact-link-blue">';
	result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">物件エントリー</span> </a>';
	result += '</li>';
	result += '</ul>';
	result += '<p class="box-bukken-contact-title">お問い合わせは「プラウド池上」プロジェクト準備室</p>';
	result += '<p class="box-bukken-contact-tel tel"><a href="#" class="js-checktel">{$TEL}</a></p>';
	result += '<p class="box-bukken-contact-text">営業時間／平日 11:00～18:00 土日祝 10:00～18:00';
	result += '<br class="only-sp">定休日／毎週水・木、毎月第2火曜日</p>';
	result += '<ul class="box-bukken-contact-list">';
	result += '<li class="box-bukken-contact-list-item"><a href="#" class="box-bukken-contact-list-link">物件概要</a></li>';
	result += '<li class="box-bukken-contact-list-item"><a href="' + layer + 'map/index.html" class="box-bukken-contact-list-link">現地案内図</a></li>';
	result += '</ul>';
	result += '</div>';
	result += '<!-- /box-bukken-contact-inner -->';
	result += '</div>';
	result += '<!-- /box-bukken-contact -->';
	result += '</div>';
	result += '<!-- /contents-property -->';
	result += '<!----　End Contents　---->';

	return result;
};
tcp.init();

/**
 * 共通化nav情報
 */
var incNav = incNav || {};
incNav.onArray = [ // 上が優先。
	{dir : '_template', index : null, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'concept', index : 1, layer : 1, new : false, off: false, next : 'access', back : 'map'},
	{dir : 'access', index : 2, layer : 1, new : false, off: false, next : 'location', back : 'concept'},
	{dir : 'location', index : 3, layer : 1, new : false, off: false, next : 'plan', back : 'access'},
	{dir : 'design', index : 4, layer : 1, new : false, off: true, next : '', back : ''},
	{dir : 'plan', index : 5, layer : 1, new : false, off: false, next : 'map', back : 'location'},
	{dir : 'ordermade', index : 6, layer : 1, new : false, off: true, next : '', back : ''},
	{dir : 'map', index : 7, layer : 1, new : false, off: false, next : 'concept', back : 'plan'},
	{dir : 'town', index : 8, layer : 1, new : false, off: true, next : '', back : ''},
	{dir : 'limited', index : 10, layer : 1, new : false, off: false, next : '', back : ''}
];

/**
 * Topic Header Package.
 */
var pathName = location.pathname;
var thp = {};
thp.onChoice = null;
thp.onArray = {};
thp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(thp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) thp.onChoice = incNav.onArray[i];
		}
		thp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(thp.onChoice == null) thp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(thp.createTag());
};
thp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < thp.onChoice.layer; i++){
		layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(thp.onChoice.index == 1) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(thp.onArray._template.new) result += 'is-new '; NEWマーク
	// if(thp.onArray._template.off) result += 'is-off '; off処理

	result += '<div class="overlay overlay--default"></div>';
	result += '<div class="overlay overlay--click"></div>';

	result += '<header class="header-property">';
	result += '<div class="header-property-box">';
	result += '<div class="header-property-top">';
	result += '<div class="header-property-logo">';
	result += '<a href="' + layer + 'index.html" class="header-property-logo-link">';
	result += '<p class="header-property-logo-title header-property-logo-small"><img src="' + layer + 'assets/imgs/logo.svg" alt="PROUD プラウド池上"></p>';
	result += '</a>';
	result += '</div>';
	result += '<div class="header-property-utility">';
	result += '<ul class="header-property-list01 only-pc">';
	result += '<li class="header-property-list01-item"><a href="#">物件概要</a></li>';
	result += '<li class="header-property-list01-item"><a href="' + layer + 'map/index.html">現地案内図</a></li>';
	result += '</ul>';
	result += '<nav class="list-property-control-wrap only-pc">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item list-property-control-item-favorite"> <a href="#" class="header-property-favorite btn-basic-favorite js-fav"><span class="btn-basic-favorite-icon">お気に入り</span></a> </li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"></i> <span class="list-property-control-text">物件エントリー</span> </a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="' + layer + 'limited/login.html" class="list-property-control-link list-property-control-link-wine">';
	result += '<i class="list-property-control-icon request"></i> <span class="list-property-control-text">物件エントリー者様限定サイト</span> </a>';
	result += '</li>';
	result += '</ul>';
	result += '</nav>';
	result += '<div class="header-property-btn-favorite only-sp"> <a href="#" class="header-property-btn-favorite-link js-fav" data-contents-number="1"><span class="header-property-btn-favorite-inner"><span>お気に入り</span></span></a> </div>';
	// result += '<p class="header-property-btn-menu js-property-menu header-brand-menu only-sp">総合メニュー</p>';
    if (thp.onChoice.index !== 10) {
        result += '<p class="header-property-btn-menu js-property-menu header-local-menu only-sp">物件メニュー</p>';
    }
	result += '</div>';
	result += '<!-- /header-property-utility -->';
	result += '<div class="header-property-menu">';
	// result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="pc-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += '</div>';
	result += '<!-- /header-property-menu-box -->';
	result += '</div>';
	result += '<!-- /header-property-menu -->';
	result += '</div>';
	result += '<!-- /header-property-top -->';
	result += '<nav class="nav-global-property brand-nav">';
	result += '<div class="header-property-menu">';
	// result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="sp-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += '</div>';
	result += '<!-- /header-property-menu-box -->';
	result += '</div>';
	result += '<!-- /header-property-menu -->';
	result += '</nav>';
	result += '<nav class="nav-global-property local-nav">';
	if (!pathName.match("limited")) {
    result += '<div class="nav-global-property-inner nav-global-property-inner-first">';
    result += '<ul class="nav-global-property-list">';
    result += '<li class="nav-global-property-item"><a href="' + layer + 'index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 0) result += "is-current ";
    result += 'js-match-height">トップ</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.concept.new) result += "is-new ";
    if (thp.onArray.concept.off) result += "is-off ";
    result += '"><a href="' + layer + 'concept/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 1) result += "is-current ";
		result += ' js-match-height">コンセプト</a></li>';
		result += '<li class="nav-global-property-item ';
		if (thp.onArray.town.new) result += "is-new ";
		if (thp.onArray.town.off) result += "is-off ";
		result += '"><a href="' + layer + 'town/index.html" class="nav-global-property-link ';
		if (thp.onChoice.index == 8) result += "is-current ";
		result += ' js-match-height">城南×東急線</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.access.new) result += "is-new ";
    if (thp.onArray.access.off) result += "is-off ";
    result += '"><a href="' + layer + 'access/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 2) result += "is-current ";
    result += ' js-match-height">アクセス</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.location.new) result += "is-new ";
    if (thp.onArray.location.off) result += "is-off ";
    result += '"><a href="' + layer + 'location/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 3) result += "is-current ";
    result += ' js-match-height">ロケーション</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.design.new) result += "is-new ";
    if (thp.onArray.design.off) result += "is-off ";
    result += '"><a href="' + layer + 'design/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 4) result += "is-current ";
    result += ' js-match-height">デザイン</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.plan.new) result += "is-new ";
    if (thp.onArray.plan.off) result += "is-off ";
    result += '"><a href="' + layer + 'plan/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 5) result += "is-current ";
    result += ' js-match-height">プラン</a></li>';
    result += '<li class="nav-global-property-item ';
    if (thp.onArray.ordermade.new) result += "is-new ";
    if (thp.onArray.ordermade.off) result += "is-off ";
    result += '"><a href="' + layer + 'ordermade/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 6) result += "is-current ";
    result += ' js-match-height">オーダーメイド</a></li>';
    result += '<li class="nav-global-property-item sp ';
    if (thp.onArray.map.new) result += "is-new ";
    if (thp.onArray.map.off) result += "is-off ";
    result += '"><a href="' + layer + 'map/index.html" class="nav-global-property-link ';
    if (thp.onChoice.index == 7) result += "is-current ";
		result += ' js-match-height">現地案内図</a></li>';
		result += '<li class="nav-global-property-item only-sp ';
		result += '"><a href="#" class="nav-global-property-link ';
		result += ' js-match-height">物件概要</a></li>';
    result += "</ul>";
    result += "</div>";
    result += '<div class="nav-global-property-inner nav-global-property-inner-second">';
    result += '<ul class="nav-global-property-list">';
    result += "</ul>";
    result += '<p class="header-property-menu-button-close"><a href="">閉じる</a></p>';
    result += "</div>";
  }
	result += '</nav>';
	result += '</div>';
	result += '<!-- /header-property -->';
	result += '</header>';
	result += '<!-- /header-property -->';

	result += '<!----　Start Contents　---->';
	result += '<div class="contents-property">';
	result += '<h1 class="ttl-property-main">【PROUD 公式ホームページ】-プラウド池上- 池上駅徒歩6分 池上駅の新築マンション</h1>';

	result += '<nav class="nav" id="accessibilityNav">';
	result += '<ul>';
	result += '<li><a href="#mainContent">コンテンツへ</a></li>';
	result += '<li><a href="#gNav">ナビゲーションへ</a></li>';
	result += '</ul>';
	result += '</nav><!-- / #accessibilityNav -->';

	return result;
};
thp.init();

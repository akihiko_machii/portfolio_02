(function($) {
  var config = function() {
    //ポップアップリンクに置換
    $(".commonPop").easyPop();
    //アンカーリンクをスムージング
    $('a[href^="#"]').smoothScroll();
    var _ua = Fourdigit.set()._ua,
      _browser = Fourdigit.set()._browser,
      _breakP = Fourdigit.set()._breakP,
      _winSize = Fourdigit.set()._winSize;
    // ブラウザを判別し、bodyにそのブラウザ名のクラスを付与
    for (var key in _browser) {
      if (_browser.hasOwnProperty(key)) {
        if (_browser[key] == true) {
          $("body").addClass(key);
        }
      }
    }
  };

  $(function() {
    /**
     * 共通系処理
     * @description
     * サイト共通で機能させる処理はここに書きます。
     */
    config();
    /**
     * VIEWPORT 切り替え
     * @description
     * TBとSPの場合で、それぞれviewportの値を切り替えます。
     */
    function updateMetaViewport() {
      var viewportContent;
      if (_ua.SP) {
        viewportContent =
          "width=device-width,initial-scale=1.0,maximum-scale=1.5,user-scalable=yes";
      } else if (_ua.TB) {
        viewportContent = "width=1200px";
      }
      document
        .querySelector("meta[name='viewport']")
        .setAttribute("content", viewportContent);
    }
    if (_ua.SP || _ua.TB) {
      window.addEventListener("load", updateMetaViewport, false);
    }
    //android tel設定
    $("a[href^=tel]").click(function() {
      location.href = $(this).attr("href");
      return false;
    });
    /**
     * PAGE TOP
     * @description
     */
    var $topBtn = $(".pagetop");
    $topBtn.on("click", function() {
      $("body,html").animate({ scrollTop: 0 }, 500, "linear");
      return false;
    });
    /**
     * SP NAVI
     * @description
     */
    var $spMenu = $("#spMenu"),
      $gNav = $("#gNav"),
      spNavFlag = false;

    if (_breakP.SP) {
      $spMenu.on("click", function() {
        if (spNavFlag) {
          $spMenu.removeClass("spMenu-active");
          $gNav.fadeOut().removeClass("gNav-active");
          spNavFlag = false;
        } else {
          $spMenu.addClass("spMenu-active");
          $gNav.fadeIn().addClass("gNav-active");
          spNavFlag = true;
        }
      });
    }

    /**
     * 各ページ固有の処理
     * 基本的にローカルにJSは置かずにcommon内で完結させる。
     */

    $(function() {
      var w = $(window).width();
      var x = 760;
      if (w <= x) {
        window.onorientationchange = function() {
          switch (window.orientation) {
            case 0:
              break;
            case 90:
              alert(
                "※当サイトを最適な状態でご覧いただくには、スマートフォンを縦にした状態でご覧ください。"
              );
              break;
            case -90:
              alert(
                "※当サイトを最適な状態でご覧いただくには、スマートフォンを縦にした状態でご覧ください。"
              );
              break;
          }
        };
      }
    });

    /**
     * ページ遷移アニメーション
     */
    var overlayFunc = {
      init: function() {
        this.setParams();
        this.prepareOverlayFadeout();
        this.bindEvent();
      },

      setParams: function() {
        this.$window = $("window");
        this.$body = $("body");
        this.$a = $("a");
        this.$overlay = $(".overlay");
        this.$overlay_df = $(".overlay--default");
        this.$overlay_clk = $(".overlay--click");
      },

      prepareBodyFadeout: function() {
        var self = this;
        self.$body.addClass("is_hide");
      },

      prepareOverlayFadeout: function() {
        var self = this;
        self.$overlay_df.fadeOut(1000);
      },

      bindEvent: function() {
        var self = this;
        self.$window.on("load", function() {
          self.prepareOverlayFadeout();
        });

        // スマホブラウザバック
        window.onpageshow = function(event) {
          if (event.persisted) {
            window.location.reload();
            self.prepareOverlayFadeout();
          }
        };

        self.$a.on("click", function() {
          var url = $(this).attr("href"),
            target = $(this).attr("target");
          self.$overlay_clk.addClass("overlay--move");
          self.$overlay.removeClass("is_hide--db");
          if (url !== "" && url.match(/#/) === null && target !== "_blank" && url !== "javascript:print();") {
            setTimeout(self.prepareBodyFadeout, 250);
            setTimeout(function() {
              location.href = url;
            }, 500);
            return false;
          } else {
            self.$overlay.addClass("is_hide--db");
          }
        });
      }
    };

    overlayFunc.init();

    /**
     * 下層ページの共通処理
     */
    var templateFunc = {
      init: function() {
        this.setParams();
        this.prepareFirstView();
        this.prepareTabSwitch();
        // this.bindEvent();
      },
      setParams: function() {
        this.$mv_blockMarkerMsk = $(
          ".mainVisual .blockMarker-box .blockMarkerMsk"
        );
        this.$line = $(".line");
        this.$lineParent = this.$line.parent("*");
        this.$headline = $(".headline--lowPage");
        this.$tab = $(".tab ul li");
        this.$tabContent = $(".tab--contents__content");
      },
      prepareFirstView: function() {
        var self = this;
        // documentのロード完了後に.aos-initがあるか判定
        $(function() {
          var conditions = self.$mv_blockMarkerMsk.hasClass("aos-init");
          var parentConditions = self.$lineParent.hasClass("aos-init");
          if (conditions) {
            var lineFunc = function(callback) {
                setTimeout(function() {
                  self.$line.fadeIn();
                  callback();
                }, 1300);
              },
              headlineFunc = function() {
                setTimeout(function() {
                  self.$headline.addClass("is_show");
                }, 1300);
              };

            lineFunc(headlineFunc.bind(null));
          } else if (parentConditions) {
            var lineFunc = function() {
              setTimeout(function() {
                self.$line.fadeIn();
              }, 1300);
            };
            lineFunc();
          }
        });
      },
      prepareTabSwitch: function() {
        var self = this;
        self.$tab.on("click", function() {
          var index = self.$tab.index(this);
          self.$tabContent.addClass("is_hide--db");
          self.$tabContent
            .eq(index)
            .removeClass("is_hide--db")
            .addClass("is_show--db");
          self.$tab.removeClass("select");
          $(this).addClass("select");
        });
      }
    };
    templateFunc.init();


    /**
     * AOSの処理
     */
    AOS.init({
      offset: 120, // 元のトリガーポイントからのオフセットをpx単位で指定出来ます。
      delay: 1000, // 0から3000までの値を50ms(0.05秒。)ずつ指定出来ます。
      duration: 400, // 0から3000までの値を50ms(0.05秒。)ずつ指定出来ます。
      easing: "ease-in", // イージング
      once: true, // スクロール後、再度アニメーション発動する場合、値をfalseに変更してください。
      anchorPlacement: "top-bottom" //要素のどの位置でアニメーションをトリガーするかを定義します。top-bottom、top-center、top-topのように2つの単語をハイフンで区切って設定し最初の単語をcenterやbottomにも設定可能です。
    });


    /**
     * 限定ページの認証
     */
    var pw = {};
    var inputData;

    pw.init = function() {
      var cookie = "";
      var allCookies = document.cookie;
      var cookieName = "ikegamiLoginededVisited" + "=";

      var cookiePosition = allCookies.indexOf(cookieName);
      if (cookiePosition > -1) {
        var startIndex = cookiePosition + cookieName.length;
        var endIndex = allCookies.indexOf(";", startIndex);
        if (endIndex == -1) endIndex = allCookies.length;
        cookie = decodeURIComponent(allCookies.substring(startIndex, endIndex));
      }

      // login.htmlのときだけパスワード判定の処理
      if ($(".wrapper").attr("id") == "limitedLogin") {
        if (cookie != "logined") {
          $("#submit").bind("click touchstart", function() {
            inputData = $("#password").val();
            pw.auth(inputData);
          });
        } else {
          document.cookie = "ikegamiLoginededVisited=logined; max-age=21600";
          location.href = "index.html";
          console.log("bbb");
        }
      } else {
        // それ以外はcookieにloginedがなければlogin.htmlに飛ばします。
        loginCheck(cookie);
      }
    };

    pw.auth = function(value) {
      var str = CybozuLabs.MD5.calc(value);
      if (str == "b17f42c1015d9fb513cbe1b602039b10") {
        document.cookie = "ikegamiLoginededVisited=logined; max-age=21600";
        location.href = "index.html";
      } else {
        alert("パスワードが間違っています");
      }
    };

    loginCheck = function(cookie) {
      if (cookie != "logined") {
        location.href = "login.html";
      }
    };
    // #md5jsがある場合のみ実行
    $(function() {
      if ($("html").hasElem("#md5JS")) {
        pw.init();
      }
    });


    switch ($(".wrapper").attr("id")) {
      case "template":
        break;
      case "index":
        if (_breakP.SP) {
          $(".usp__list").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerPadding: "18%",
            centerMode: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 800,
            dots: true,
            infinite: true,
            focusOnSelect: true,
            cssEase: 'cubic-bezier(.38,-0.67,0,1)'
          });
        }

        $(function(){
          /*時間があれば関数化させたい*/
            var $imageScroll01 = $('.imageScroll--01 .sp-nowrap');
            var $imageScroll02 = $('.imageScroll--02 .sp-nowrap');
            var flag01 = true;
            var flag02 = true;
            var imageScroll01W, imageScroll02W;

            imageScroll01W = $imageScroll01.find('.clm--01').width();
            imageScroll02W = $imageScroll02.find('.clm--01').width();

            $imageScroll01.scrollLeft(imageScroll01W);
            $imageScroll02.scrollLeft(0);

            $(window).on('scroll load resize', function(){
              if (_breakP.SP) {
                if (flag01) {
                    if ($imageScroll01.parent('.imageScroll--01').hasClass('aos-animate')) {
                        setTimeout(function () {
                            $imageScroll01.animate({scrollLeft: 0}, 1500);
                        }, 2000);
                        flag01 = false;
                    }
                }
                if (flag02) {
                    if ($imageScroll02.parent('.imageScroll--02').hasClass('aos-animate')) {
                        setTimeout(function () {
                            $imageScroll02.animate({scrollLeft: imageScroll02W}, 1500);
                        }, 2000);
                        flag02 = false;
                    }
                }
              }
            });
        });
        break;
      case "access":
        $(".rootSearch__search").on("click", function() {
          var _obj = $(this).parent();
          var start = "東急池上線「池上」駅";
          var goal = $("input", _obj).val();
          var e_start = encodeURIComponent(start);
          var e_goal = encodeURIComponent(goal);
          var gmapURL =
            "http://maps.google.com/maps?saddr=" +
            e_start +
            "&daddr=" +
            e_goal +
            "&dirflg=r";
          var error_msg_text = "駅名を入力して下さい";

          if (goal === "") {
            swal("", error_msg_text, "error");
          } else {
            window.open(gmapURL, "_blank");
            ga("send", {
              hitType: "event",
              // 'eventCategory': 'rootSearch-mst',
              eventCategory: "rootSearch-ikegami",
              eventAction: "click",
              eventLabel: goal
            });
          }
        });

        break;
      case "concept":
        $(function() {
          var historyTableFunc = function() {
            var $historyTable  = $(".historyTable");

            var innerFunc = function () {
              return $historyTable;
            }

            return innerFunc;
          };

          var myHistoryTable = historyTableFunc();


          $(window).on("load resize", function() {
            var winW = $(window).width();
            if (winW < 736) {
              if ($(".historyTable").find(".historyTable__sp").length === 0) {
                historyTable();
              }
            } else {
              if ($(".historyTable").find(".historyTable__sp").length !== 0) {
                var timer = false;
                $(window).resize(function() {
                  if (timer !== false) {
                    clearTimeout(timer);
                  }
                  timer = setTimeout(function() {
                    location.reload();
                    $(".historyTable").replaceWith(myHistoryTable());
                  }, 50);
                });
              }
            }
          });
        });

        function historyTable() {
          var $historyTableLeft = $(".historyTable__clm--left");
          var $historyTableRight = $(".historyTable__clm--right");

          var $itemLeft = $historyTableLeft.find(".historyTable__item");
          var $itemRight = $historyTableRight.find(".historyTable__item");

          var $itemLength;
          var $items = [];

          if ($itemLeft.length > $itemRight.length) {
            $itemLength = $itemLeft.length;
          } else {
            $itemLength = $itemRight.length;
          }

          for (var i = 0; i < $itemLength; i++) {
            if ($itemLeft.eq(i).length !== 0) {
              $items.push($itemLeft.eq(i));
            }
            if ($itemRight.eq(i).length !== 0) {
              $items.push($itemRight.eq(i));
            }
          }

          $(".historyTable").html('<ul class="historyTable__sp"></ul>');
          $(".historyTable__sp").html($items);
        }
        break;

      case "location":
        $(window).on("load resize", function() {
          var winW = $(window).width();
          /*if (winW < 736) {*/
            var $img = $(".imgViewer").imgViewer2();
          /*}*/
        });
        break;
      default:
        break;
    }
  });
})(jQuery);

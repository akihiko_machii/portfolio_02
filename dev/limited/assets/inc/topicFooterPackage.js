/**
 * Topic Footer Package.
 */
var tfp = {};
tfp.onChoice = null;
tfp.onArray = {};
tfp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(tfp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) tfp.onChoice = incNav.onArray[i];
		}
		tfp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(tfp.onChoice == null) tfp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(tfp.createTag());


};
tfp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < tfp.onChoice.layer; i++){
			layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(tfp.onChoice.index == 0) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(tfp.onArray._template.off) result += 'is-off '; off処理
	// next back
	// if(tfp.onChoice.back != '') result += '<li class="back"><a href="../' + tfp.onChoice.back + '/index.html">BACK</a></li>';
	// if(tfp.onChoice.next != '') result += '<li class="next"><a href="../' + tfp.onChoice.next + '/index.html">NEXT</a></li>';

result += '<!----　Start Footer　---->';
result += '<footer class="footer-property">';
result += '<p class="btn-pagetop-property"><a href="#" class="js-scroll">ページトップへ</a></p>';
result += '<!-- /btn-pagetop-property -->';
result += '<div class="footer-copyright">';
result += '<a href="https://www.nomura-re.co.jp/" target="_blank">';
result += '<figure class="footer-copyright-image"> <img src="https://www.proud-web.jp/assetslet/img/logo_mainfooter.png" alt="あしたを、つなぐ 野村不動産グループ 野村不動産"> </figure>';
result += '</a>';
result += '<p class="footer-copyright-text">Copyright &copy; Nomura Real Estate Development Co., Ltd. All Rights Reserved. </p>';
result += '</div>';
result += '</footer>';
result += '<!----　End Footer　---->';

	return result;
};
tfp.init();
/**
 * Topic CV Package.
 */
var tcp = {};
tcp.onChoice = null;
tcp.onArray = {};
tcp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(tcp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) tcp.onChoice = incNav.onArray[i];
		}
		tcp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(tcp.onChoice == null) tcp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(tcp.createTag());


};
tcp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < tcp.onChoice.layer; i++){
			layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(tcp.onChoice.index == 0) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(tcp.onArray._template.off) result += 'off '; off処理
	// next back
	// if(tcp.onChoice.back != '') result += '<li class="back"><a href="../' + tcp.onChoice.back + '/index.html">BACK</a></li>';
	// if(tcp.onChoice.next != '') result += '<li class="next"><a href="../' + tcp.onChoice.next + '/index.html">NEXT</a></li>';

	result += '<nav class="list-property-control-wrap only-sp">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item contact">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon contact"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="お問い合わせ"></i> <span class="list-property-control-text">お問い合わせ</span> </a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="../map/index.html" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon localmap"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="現地案内図"></i>';
	result += '<span class="list-property-control-text">現地案内図</span>';
	result += '</a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="https://www.proud-web.jp/module/material/SiryoOrderTop.xphp?code_no=011229" target="_blank" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"><img src="https://www.proud-web.jp/general/img/ico_book_01_sp.png" class="only-sp" alt="物件エントリー"></i> <span class="list-property-control-text">物件エントリー</span> </a>';
	result += '</li>';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="#" class="list-property-control-link list-property-control-link-green">';
	// result += '<i class="list-property-control-icon reservation01"><img src="https://www.proud-web.jp/general/img/ico_reservation_01_sp.png" class="only-sp" alt="来場予約"></i> <span class="list-property-control-text">来場予約</span> </a>';
	// result += '</li>';
	result += '<!-- ボタン数は最大5つまでなのでコメントアウトで調整してください。';
	result += '<li class="list-property-control-item">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-green">';
	result += '<i class="list-property-control-icon reservation02"><img src="https://www.proud-web.jp/general/img/ico_reservation_02_sp.png" class="only-sp" alt="再来場予約"></i> <span class="list-property-control-text">再来場予約</span> </a>';
	result += '</li>-->';
	result += '<li class="list-property-control-item">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-wine">';
	result += '<i class="list-property-control-icon request"><img src="https://www.proud-web.jp/general/img/ico_request_01_sp.png" class="only-sp" alt="物件エントリー者様限定サイト"></i> <span class="list-property-control-text">物件エントリー者様限定サイト</span> </a>';
	result += '</li>';
	result += '</ul>';
	result += '</nav>';

	// 以下は必要があればコメントアウトを取って利用する
	// result += '<div class="box-bukken-contact">';
	// result += '<div class="box-bukken-contact-inner">';
	// result += '<ul class="list-bukken-contact">';
	// result += '<li class="list-bukken-contact-item favorite"> <a href="#" class="list-bukken-contact-link btn-basic-favorite js-fav"><span class="btn-basic-favorite-icon">お気に入り</span></a> </li>';
	// result += '<li class="list-bukken-contact-item entry">';
	// result += '<a href="https://www.proud-web.jp/module/material/SiryoOrderTop.xphp?code_no=011229" target="_blank" class="list-bukken-contact-link list-bukken-contact-link-blue">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">物件エントリー</span> </a>';
	// result += '</li>';
	// result += '<li class="list-bukken-contact-item reservation01">';
	// result += '<a href="#" class="list-bukken-contact-link list-bukken-contact-link-green">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">来場予約</span> </a>';
	// result += '</li>';
	// result += '<li class="list-bukken-contact-item reservation02">';
	// result += '<a href="#" class="list-bukken-contact-link list-bukken-contact-link-green">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">再来場予約</span> </a>';
	// result += '</li>';
	// result += '<li class="list-bukken-contact-item request">';
	// result += '<a href="#" class="list-bukken-contact-link list-bukken-contact-link-wine">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">資料請求者限定サイト</span> </a>';
	// result += '</li>';
	// result += '</ul>';
	// result += '<p class="box-bukken-contact-title">お問い合わせは「プラウド池上」販売準備室</p>';
	// result += '<p class="box-bukken-contact-tel tel"><a href="tel:0120-000-000" class="js-checktel">0120-000-000</a></p>';
	// result += '<p class="box-bukken-contact-text">営業時間／平日 11:00～18:00 土日祝 10:00～18:00';
	// result += '<br class="only-sp">定休日／毎週水・木、毎月第2火曜日</p>';
	// result += '<ul class="box-bukken-contact-list">';
	// result += '<li class="box-bukken-contact-list-item"><a href="#" class="box-bukken-contact-list-link">物件概要</a></li>';
	// result += '<li class="box-bukken-contact-list-item"><a href="#" class="box-bukken-contact-list-link">現地案内図</a></li>';
	// result += '</ul>';
	// result += "</div><!-- /box-bukken-contact-inner -->";
	// result += "</div><!-- /box-bukken-contact -->";

	result += '</div><!-- /contents-property -->';
	result += '<div class="box-bukken">';
	result += '<div class="box-bukken-inner">';
	result += '<figure class="bnr-bukken"><a href="https://www.proud-web.jp/proud/kizuna/" target="_blank">';
	result += '<img src="https://www.proud-web.jp/general/img/home/bnr_property_01_pc.png" class="pc" alt="永きにわたり結んでいく、お客さまとの絆。 日々の暮らしをより上質で豊かにする 野村不動産グループカスタマークラブ">';
	result += '<img src="https://www.proud-web.jp/general/img/home/bnr_property_01_sp.png" class="sp" alt="永きにわたり結んでいく、お客さまとの絆。 日々の暮らしをより上質で豊かにする 野村不動産グループカスタマークラブ">';
	result += '</a></figure>';
	result += '</div>';
	result += '<!-- /box-bukken-inner -->';
	result += '</div>';
	result += '<!-- /box-bukken -->';
	result += '<!----　End Contents　---->';

	return result;
};
tcp.init();

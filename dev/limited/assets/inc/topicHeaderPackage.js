/**
 * 共通化nav情報
 */
var incNav = incNav || {};
incNav.onArray = [ // 上が優先。
	{dir : '_template', index : null, layer : 1, new : false, off: false, next : '', back : ''},
];

/**
 * Topic Header Package.
 */
var thp = {};
thp.onChoice = null;
thp.onArray = {};
thp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(thp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) thp.onChoice = incNav.onArray[i];
		}
		thp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(thp.onChoice == null) thp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(thp.createTag());
};
thp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < thp.onChoice.layer; i++){
		layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(thp.onChoice.index == 1) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(thp.onArray._template.new) result += 'is-new '; NEWマーク
	// if(thp.onArray._template.off) result += 'is-off '; off処理

	// result += '<div class="overlay overlay--default"></div>';
	// result += '<div class="overlay overlay--click"></div>';

	result += '<header class="header-property">';
	result += '<div class="header-property-box">';
	result += '<div class="header-property-top">';
	result += '<div class="header-property-logo">';
	result += '<a href="../index.html" class="header-property-logo-link">';
	result += '<p class="header-property-logo-title header-property-logo-small"><img src="../assets/imgs/logo.svg" alt="PROUD {$bukkenmei$}"></p>';
	result += '</a>';
	result += '</div>';
	result += '<div class="header-property-utility">';
	result += '<ul class="header-property-list01 only-pc">';
	result += '<li class="header-property-list01-item"><a href="https://www.proud-web.jp/module/material/SiryoOrderTop.xphp?code_no=011229" target="_blank">物件概要</a></li>';
	result += '<li class="header-property-list01-item"><a href="../map/index.html">現地案内図</a></li>';
	result += '</ul>';
	result += '<nav class="list-property-control-wrap only-pc">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item list-property-control-item-favorite"> <a href="#" class="header-property-favorite btn-basic-favorite js-fav"><span class="btn-basic-favorite-icon">お気に入り</span></a> </li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="https://www.proud-web.jp/module/material/SiryoOrderTop.xphp?code_no=011229" target="_blank" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"></i> <span class="list-property-control-text">物件エントリー</span> </a>';
	result += '</li>';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="#" class="list-property-control-link list-property-control-link-green">';
	// result += '<i class="list-property-control-icon reservation01"></i> <span class="list-property-control-text">来場予約</span> </a>';
	// result += '</li>';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="#" class="list-property-control-link list-property-control-link-green">';
	// result += '<i class="list-property-control-icon reservation02"></i> <span class="list-property-control-text">再来場予約</span> </a>';
	// result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="#" class="list-property-control-link list-property-control-link-wine">';
	result += '<i class="list-property-control-icon request"></i> <span class="list-property-control-text">物件エントリー者様限定サイト</span> </a>';
	result += '</li>';
	result += '</ul>';
	result += '</nav>';
	result += '<div class="header-property-btn-favorite only-sp"> <a href="#" class="header-property-btn-favorite-link js-fav" data-contents-number="1"><span class="header-property-btn-favorite-inner"><span>お気に入り</span></span></a> </div>';
	result += '<p class="header-property-btn-menu js-property-menu header-brand-menu only-sp">総合メニュー</p>';
	result += '</div>';
	result += '<!-- /header-property-utility -->';
	result += '<div class="header-property-menu">';
	result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="pc-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += "</div><!-- /header-property-menu-box -->";
	result += "</div><!-- /header-property-menu -->";
	result += "</div><!-- /header-property-top -->";
	result += '<nav class="nav-global-property brand-nav">';
	result += '<div class="header-property-menu">';
	result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="sp-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += "</div><!-- /header-property-menu-box -->";
	result += "</div><!-- /header-property-menu -->";
	result += '</nav>';
	result += "</div><!-- /header-property -->";
	result += "</header><!-- /header-property -->";

	result += '<!---- Start Contents ---->';
	result += '<div class="contents-property">';
	result += '<h1 class="ttl-property-main">【PROUD 公式HP】―プラウド池上― ○○線「○○」駅徒歩00分｜野村不動産の新築マンション</h1>';
	result += '<nav class="nav" id="accessibilityNav">';
	result += '<ul>';
	result += '<li><a href="#mainContent">コンテンツへ</a></li>';
	result += '<li><a href="#gNav">ナビゲーションへ</a></li>';
	result += '</ul>';
	result += '</nav><!-- / #accessibilityNav -->';

	return result;
};
thp.init();
